## Git

* [Démarrer avec Git](https://www.lirmm.fr/~pvalicov/Cours/archives/Aix/M2104/Demarrer%20avec%20Git)
* [mini tuto](https://gitlabinfo.iutmontp.univ-montp2.fr/valicov/tutoGit1ereAnnee) pour GitLab à l'IUT Montpellier-Sète
* [Git-it](https://github.com/jlord/git-it-electron) - application Desktop pour apprendre Git avec GitHub
* [Tutoriel officiel Git](https://git-scm.com/docs/gittutorial)
* [Git Cheat-sheet](https://education.github.com/git-cheat-sheet-education.pdf)


## Support cours (versions condensées, volontairement incomplètes)
* [Généralités : les objets, Java, notation UML](https://www.lirmm.fr/~pvalicov/Cours/dev-objets/Generalites_x4.pdf)
* [Héritage et Polymorphisme](https://www.lirmm.fr/~pvalicov/Cours/dev-objets/Heritage_Polymorphisme_x4.pdf)
* [Généricité et Structures de Données](https://www.lirmm.fr/~pvalicov/Cours/dev-objets/Genericite_Structures_de_Donnees_x4.pdf)
* [Gestion d'exceptions](https://www.lirmm.fr/~pvalicov/Cours/dev-objets/Exceptions_x4.pdf)

## SAE 2.1

* [Pokémon TCG](https://fr.wikipedia.org/wiki/Jeu_de_cartes_%C3%A0_collectionner_Pok%C3%A9mon).
   * Les [règles du jeu](https://www.pokemon.com/static-assets/content-assets/cms2-fr-fr/pdf/trading-card-game/rulebook/pal_rulebook_fr.pdf)
   * Deux decks à implémenter :
      * [Envolée Orageuse](https://www.pokepedia.fr/Envol%C3%A9e_Orageuse)
      * [Flamme Incessante](https://www.pokepedia.fr/Flamme_Incessante)

